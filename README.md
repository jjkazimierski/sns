# Laravel Boilerplate
> A Laravel boilerplate project visually empowered by **White Dashboard** from *Creative Tim*.

## Table of contents
* [General Information](#general-information)
* [Screenshots](#screenshots)
* [Requirements](#requirements)
* [Installation](#installation)
* [Licenses](#licenses)
* [Status](#status)
* [Contact](#contact)

## General Information

...

## Screenshots

## Requirements

#### PHP
* **Version:** 7.4.2

#### Laravel
* **Version:** 6.13.0
* **License:** https://laravel-guide.readthedocs.io/en/latest/license
* **Docs:** https://laravel.com/docs/6.x
* **URL:** https://laravel.com/ 
* **Retrieved:** 31/01/2020

#### Composer
* **Version:** 1.9.0
* **License:** https://github.com/composer/composer/blob/master/LICENSE
* **Docs:** https://getcomposer.org/doc/
* **URL:** https://getcomposer.org/ 
* **Retrieved:** 31/01/2020

## Setup

1. Create a database locally with encoding: *utf8mb4_unicode_ci*.
2. Rename *.env.example* file to *.env* inside your project root and fill the database information.
3. Open the console and `cd` your project root directory.
4. `npm install`
5. `composer install`
6. `php artisan key:generate`
7. `php artisan migrate`
8. `php artisan db:seed`
9. `php artisan serve`

## Licenses

### White Dashboard Laravel

> MIT License
> 
> Copyright (c) 2017 Creative Tim [https://www.creative-tim.com](https://www.creative-tim.com)
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.
> 
> -- <cite>[creative-tim.com][1]</cite>

## Status
Project is: _in progress_.

## Contact
* Created by [Jacek Kazimierski](mailto:j.j.kazimierski@gmail.com)

[1]:https://github.com/timcreative/freebies/blob/master/LICENSE.md