<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url(../argon/video/bg2.jpeg); background-size: cover; background-position: center top;">

  <!-- <video class="video-background" no-controls autoplay loop muted src="{{ asset('argon') }}/video/bg1.mp4" poster="{{ asset('argon') }}/video/bg1.jpg"></video> -->
  <!-- Mask -->
  <span class="mask bg-gradient-default opacity-6"></span>
  <!-- <span class="mask bg-gradient-dark opacity-4"></span> -->
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center bg-text-shadow">
        <div class="row">
            <div class="col-md-12 {{ $class ?? '' }}">
                <h1 class="display-2 text-white">{{ $title }}</h1>
                @if (isset($description) && $description)
                    <p class="text-white mt-0 mb-5">{{ $description }}</p>
                @endif
            </div>
        </div>
    </div>
</div>
