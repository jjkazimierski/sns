<nav class="navbar navbar-dark fixed-bottom align-items-center justify-content-between" style="margin-left:250px;">
    <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
            <img src="{{ asset('argon') }}/img/brand/logo.png" style="height:40px;"/>
        </div>
    </div>

    <div class="col-xl-6 text-right" style="font-size:12px;">
      <ul class="nav nav-footer justify-content-center justify-content-xl-end">
          <li class="nav-item" style="margin-right:36px;">
            <strong>Obsługa klienta</strong><br>
      Tel.: +48 22 450 26 26<br>
      rezerwacja@sunandsnow.pl<br><br>
          </li><br>
          <li class="nav-item">
            <strong>Godziny otwarcia</strong><br>
            Pon. - Pią.: 8:00 - 20:00<br>
            Sob. - Niedz.: 9:00 - 17:00
          </li>
      </ul>


    </div>
</nav>
