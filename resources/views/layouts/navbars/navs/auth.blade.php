<!-- Top navbar -->
<nav class="navbar sticky-top navbar-light" id="navbar-main" style="background-color:#FFF;padding: 6px 0 0 0;max-height:56px;">
    <div class="container-fluid" style="padding-left:10px !important; max-height:56px;">
        <!-- Brand -->
        <!-- <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="{{ route('home') }}">{{ __('Dashboard') }}</a> -->
        <div class="d-flex flex-row justify-content-start ml-4">
            <nav class="breadcrumb" style="background-color:#fff;">
                <a class="breadcrumb-item text-primary h4 mb-0 d-none d-lg-inline-block small" style="color:#1c4269 !important;" href="#">Sun & Snow</a>
                <span class="breadcrumb-item active small">Apartamenty</span>
            </nav>
        </div>
        <div style="margin-top:-10px;">
          <ul class="navbar-nav align-items-start d-flex flex-row">


            <button type="button" class="btn btn-primary mr-4" style="background-color:#FFF !important;border-color:#FFF !important;box-shadow:none;">
              <span class="badge badge-md badge-circle badge-floating badge-primary border-white text-center" style="padding:10px!important;">7</span>
              <span class="mr-2 d-none d-lg-inline" style="font-size:18px !important; color:#234176;"><i _ngcontent-stj-c5="" class="ni ni-bell-55"></i></span>
            </button>
            <li class="nav-item no-arrow mr-4 mt-1">
              <a class="nav-link text-default" href="#">
                <span class="mr-2 d-none d-lg-inline" style="font-size:18px !important; color:#234176;"><i class="fas fa-user"></i></span>
              </a>
            </li>
            <li class="nav-item no-arrow ml-2 mt-1">
              <a class="nav-link text-default" href="#">
                <span class="mr-2 d-none d-lg-inline" style="font-size:18px !important; color:#234176;"><i class="fas fa-power-off"></i></span>
              </a>
            </li>

          </ul>
        </div>
        <!-- User -->
        <!-- <ul class="navbar-nav align-items-center d-none d-md-flex"> -->

    </div>
</nav>
