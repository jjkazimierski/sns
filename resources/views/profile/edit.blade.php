@extends('layouts.app', ['title' => __('User Profile')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Apartamenty'),
        'description' => __('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dapibus sed odio in mattis. Curabitur dapibus velit et lorem porttitor pellentesque. Morbi lobortis pulvinar convallis.'),
        'class' => 'col-lg-7'
    ])

    <div class="container-fluid mt--7">
        <div class="row">

          <div class="cardzoom col-xl-4">
              <div class="card cardimg shadow" style="background-image: linear-gradient(135deg, rgba(0, 0, 0, 0.7) 0%, rgba(0, 0, 0, 0.4) 35%, rgba(0, 0, 0, 0)), url(../argon/img/44.jpg); background-size: cover; background-position: center top;">
                  <div class="card-header border-0" style="background-color:transparent !important;">
                      <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-light ls-1 mb-1">Szklarska Poręba</h6>
                                <h2 class="text-white mb-0 bg-text-shadow">Sun&Snow Resorts, Apartament 111</h2>
                            </div>
                        </div>
                  </div>
                  <div class="card-body" style="height:120px;">
                  </div>
                  <div class="card-footer d-flex flex-row-reverse" style="background-color:transparent !important; padding:0 15px 15px 0px;">
                    <a href="#!" class="btn btn-sm btn-primary" style="background-color:#ef9a18; border-color: #ef9a18;">Zobacz szczegóły</a>
                  </div>
              </div>
          </div>

          <div class="cardzoom col-xl-4">
              <div class="card cardimg shadow" style="background-image: linear-gradient(135deg, rgba(0, 0, 0, 0.7) 0%, rgba(0, 0, 0, 0.4) 35%, rgba(0, 0, 0, 0)), url(../argon/img/55.jpg); background-size: cover; background-position: center top;">
                  <div class="card-header border-0" style="background-color:transparent !important;">
                      <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-light ls-1 mb-1">Karpacz</h6>
                                <h2 class="text-white mb-0 bg-text-shadow">Triventi Mountain Residence,<br> Karkonoska 13/40</h2>
                            </div>
                        </div>
                  </div>
                  <div class="card-body" style="height:120px;">
                  </div>
                  <div class="card-footer d-flex flex-row-reverse" style="background-color:transparent !important; padding:0 15px 15px 0px;">
                    <a href="#!" class="btn btn-sm btn-primary" style="background-color:#ef9a18; border-color: #ef9a18;">Zobacz szczegóły</a>
                  </div>
              </div>
          </div>

          <div class="cardzoom col-xl-4">
              <div class="card cardimg shadow" style="background-image: linear-gradient(135deg, rgba(0, 0, 0, 0.7) 0%, rgba(0, 0, 0, 0.4) 35%, rgba(0, 0, 0, 0)), url(../argon/img/66.jpg); background-size: cover; background-position: center top;">
                  <div class="card-header border-0" style="background-color:transparent !important;">
                      <div class="row align-items-center">
                            <div class="col">
                                <h6 class="text-uppercase text-light ls-1 mb-1">Zakopane</h6>
                                <h2 class="text-white mb-0 bg-text-shadow">Resorts Lipki Park, Studio A 06</h2>
                            </div>
                        </div>
                  </div>
                  <div class="card-body" style="height:120px;">
                  </div>
                  <div class="card-footer d-flex flex-row-reverse" style="background-color:transparent !important; padding:0 15px 15px 0px;">
                    <a href="#!" class="btn btn-sm btn-primary" style="background-color:#ef9a18; border-color: #ef9a18;">Zobacz szczegóły</a>
                  </div>
              </div>
          </div>

        </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection
