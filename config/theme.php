<?php

return [

  /*
|--------------------------------------------------------------------------
| Application Layout Name
|--------------------------------------------------------------------------
|
| This value is the name of your application's layout.
|
*/

  'default' => env('APP_THEME', 'default')
];
