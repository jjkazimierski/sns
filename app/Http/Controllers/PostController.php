<?php

namespace App\Http\Controllers;

use DB;
use App\Post;

class PostController extends Controller
{

    function show($slug)
    {
        return view('app/post', [
            'post' => Post::where('slug', $slug)->firstOrFail()
        ]);
    }
}
